#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 12 10:42:23 2020

@author: Diakhére Gueye 2020
@author: Tobias Rasmussen 2021
"""

import sys
import random
import threading
from PIL.Image import Image
from PIL.ImageQt import ImageQt 
from PyQt5 import QtWidgets, uic, QtGui, QtCore
from PyQt5.QtWidgets import QLabel, QPushButton, QPlainTextEdit, QLineEdit
import utils.GameConstants as GameConstants
import utils.DataContainer
import utils.Generator

""" Additional files needed """
initDialogFile = "./ui/initialization_interface.ui" # File path for initial popup dialog box
mainWindowFile = "./ui/MainWindow_interface.ui" # File path for main window GUI
Ui_InitDialog, QtBaseClass = uic.loadUiType(initDialogFile)
Ui_MainWindow, QtBaseClass = uic.loadUiType(mainWindowFile)

""" THE MAIN WINDOW CLASS""" 

class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):

    def __init__(self):
        """Constructor method"""
        super().__init__()
        QtWidgets.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.setWindowTitle("Quantum Mønster")
        
        self.turns = 0 # type : int
        self.points = 0 # type : int
        self.current_theory = 0 # type : int. 0 refers to piflux, 1 refers to strings, 2 refers to random
        self.current_game_mode = None # type : str
        self.chooseable_theories = GameConstants.THEORYCODES # type : list Which theories can be chosen
        self.highligt_answer = False # type : bool
        self.single_image_mode = True # type : bool
        self.option_screen = False # type : bool
        self.doping = 0 # type : float. 4.5 easy, 9.0 hard 
        self.deviation_mode = False # type : bool 
        self.is_showing_histogram = False # type : bool
        self.training_mode = True # type : bool
        self.drawing_mode_colors = GameConstants.ALT_MODE_REGULAR_COLORS # type : list. Button colors in alt mode
        self.examples_are_left_side = True

        self.ImageGenerator = utils.Generator.Generator()
        self.DataContainer = utils.DataContainer.DataContainer()

        # Alternative mode Buttons
        self.drawing_mode_buttons_list = [
            self.button_0_0, self.button_0_1, self.button_0_2, self.button_0_3, self.button_0_4, self.button_0_5,
            self.button_1_0, self.button_1_1, self.button_1_2, self.button_1_3, self.button_1_4, self.button_1_5,
            self.button_2_0, self.button_2_1, self.button_2_2, self.button_2_3, self.button_2_4, self.button_2_5,
            self.button_3_0, self.button_3_1, self.button_3_2, self.button_3_3, self.button_3_4, self.button_3_5,
            self.button_4_0, self.button_4_1, self.button_4_2, self.button_4_3, self.button_4_4, self.button_4_5,
            self.button_5_0, self.button_5_1, self.button_5_2, self.button_5_3, self.button_5_4, self.button_5_5]
        # Give buttons a color
        i,j,k = 0, 0, 0
        self.button_dict = {}
        self.buttons_for_reset = {}
        for button in self.drawing_mode_buttons_list:
            k = (i + j ) % 2 # 0 = up, 1 = down
            self.button_dict[button] = [i, j, k + 1]
            self.buttons_for_reset[button] = [i,j,k + 1]
            j = j + 1
            if j > 5:
                j = 0
                i = i + 1
        
        # Pre-user login / load saved sessions
        self.LoadSaveGameLabel = self._make_label("Load a previous session or make a new one", size=(650,60), move=(100, 50), style="font-size: 32px; color:black;", hide=True)
        self.SavedGameButton1 = self._make_button("Empty slot", size=GameConstants.LOAD_GAME_BUTTON_SIZE, move=(150, 150), style=GameConstants.LOAD_GAME_BUTTON_STYLE, hide=True)
        self.SavedGameButton2 = self._make_button("Empty slot", size=GameConstants.LOAD_GAME_BUTTON_SIZE, move=(150, 250), style=GameConstants.LOAD_GAME_BUTTON_STYLE, hide=True)
        self.SavedGameButton3 = self._make_button("Empty slot", size=GameConstants.LOAD_GAME_BUTTON_SIZE, move=(150, 350), style=GameConstants.LOAD_GAME_BUTTON_STYLE, hide=True)
        self.NewLoginButton = self._make_button("New login", size=GameConstants.LOAD_GAME_BUTTON_SIZE, move=(450, 300), style=GameConstants.LOAD_GAME_BUTTON_STYLE, hide=True)
        self.NewLoginButton.clicked.connect(self.show_login_screen)

        # User login
        self.LoginNameLabel = self._make_label("Name", move=(380,20), style="font-size: 24px; color:black", hide=True)
        self.LoginPhysicistLabel = self._make_label("Are you a physicist?", size=(250,40), move=(310,110), style="font-size: 24px; color:black", hide=True)
        self.NameInput = self._make_lineEdit(size=GameConstants.INPUT_LINE_SIZE, move=(253,60), style=GameConstants.INPUT_LINE_STYLE)
        self.NameInput.hide()  
        self.EndUserButton = self._make_button("Continue", move=(500,550), style="font-size: 18px;background: white; color:black", hide=True)
        self.EndUserButton.clicked.connect(self.save_login_info)     
        self.PhysicistButtonYes = self._make_button("Yes", size=GameConstants.LEVEL_BUTTON_SIZE, move=(260, 170), style=GameConstants.BUTTON_STYLE_NOT_TOGGLED, hide=True)
        self.PhysicistButtonYes.clicked.connect(lambda : self.set_player_is_physicist(True))
        self.PhysicistButtonNo = self._make_button("No", size=GameConstants.LEVEL_BUTTON_SIZE, move=(440, 170), style=GameConstants.BUTTON_STYLE_NOT_TOGGLED, hide=True)
        self.PhysicistButtonNo.clicked.connect(lambda : self.set_player_is_physicist(False))
        self.PhysicistLevelLabel = self._make_label("Physicist level in field", size=(250, 40), move=(310, 260), style="font-size: 24px; color:black;", hide=True)
        self.PhysicistButtonGeneral = self._make_button("Not expert \nin field", size=GameConstants.LEVEL_BUTTON_SIZE, move=(180, 300), style="font-size: 20px; color:black; background: grey;", hide=True)
        self.PhysicistButtonGeneral.setEnabled(False)
        self.PhysicistButtonGeneral.clicked.connect(lambda : self.set_player_physicist_level("general"))
        self.PhysicistButtonTheorist = self._make_button("Field expert \n(theorist)", size=GameConstants.LEVEL_BUTTON_SIZE, move=(350, 300), style="font-size: 18px; color:black; background: grey;", hide=True)
        self.PhysicistButtonTheorist.setEnabled(False)
        self.PhysicistButtonTheorist.clicked.connect(lambda : self.set_player_physicist_level("theorist"))
        self.PhysicistButtonExperimentalist = self._make_button("Field expert \n(experimentalist)", size=GameConstants.LEVEL_BUTTON_SIZE, move=(520, 300), style="font-size: 18px; color:black; background: grey;", hide=True)
        self.PhysicistButtonExperimentalist.setEnabled(False)
        self.PhysicistButtonExperimentalist.clicked.connect(lambda : self.set_player_physicist_level("experimentalist"))

        # Main screen
        self.ChooseModeLabel = self._make_label("Choose a game mode :", size=(400,80), move=(240,0),style="font-size: 32px; color:black", hide=True) 
        self.ModeButtonClassic = self._make_button("Classic Mode", size=GameConstants.MODE_BUTTON_SIZE, move=(290,150), style=GameConstants.MODE_BUTTON_STYLE, hide=True)
        self.ModeButtonClassic.clicked.connect(lambda : self.setup_gamemode(GameConstants.GAME_MODE_CLASSIC))
        self.ModeButtonDrawing = self._make_button("Drawing Mode", size=GameConstants.MODE_BUTTON_SIZE, move=(10,275), style=GameConstants.MODE_BUTTON_STYLE, hide=True)
        self.ModeButtonDrawing.clicked.connect(lambda : self.setup_gamemode(GameConstants.GAME_MODE_DRAWING))
        self.ModeButtonSprinkled = self._make_button("Classic+ Mode", size=GameConstants.MODE_BUTTON_SIZE, move=(10, 150), style=GameConstants.MODE_BUTTON_STYLE, hide=True)
        self.ModeButtonSprinkled.clicked.connect(lambda : self.setup_gamemode(GameConstants.GAME_MODE_CLASSICPLUS))
        self.ModeButtonComparison = self._make_button("Comparison Mode", size=GameConstants.MODE_BUTTON_SIZE, move=(570, 150), style=GameConstants.MODE_BUTTON_STYLE, hide=True)
        self.ModeButtonComparison.clicked.connect(lambda : self.setup_gamemode(GameConstants.GAME_MODE_COMPARISON))
        self.ModeButtonHeisenberg = self._make_button("Heisenberg Mode", size=GameConstants.MODE_BUTTON_SIZE, move=(570, 275), style=GameConstants.MODE_BUTTON_STYLE, hide=True)
        self.ModeButtonHeisenberg.clicked.connect(lambda : self.setup_gamemode(GameConstants.GAME_MODE_HEISENBERG))
        self.ModeButtonExamples = self._make_button("See examples", size=GameConstants.MODE_BUTTON_SIZE, move=(290, 275), style=GameConstants.MODE_BUTTON_STYLE, hide=True)
        self.ModeButtonExamples.clicked.connect(lambda : self.setup_gamemode(GameConstants.GAME_MODE_EXAMPLES))
        self.OptionsButton = self._make_button("Options", size=GameConstants.MODE_BUTTON_SIZE, move=(10, 500), 
                                                style=GameConstants.MODE_BUTTON_STYLE, hide=True)
        self.OptionsButton.clicked.connect(self.toggle_options_screen)
        
        # Options screen
        self.OptionsScreenLabel = self._make_label("Options", size=(400,80), move=(330, 10), style="font-size: 32px;color:black", hide=True)
        self.ChooseLevelLabel = self._make_label("Choose difficulty", size=(180,40), move=(330, 385), style="font-size: 24px;color:black", hide=True, align=True)
        self.LevelButtonEasy = self._make_button("Easy", size=GameConstants.LEVEL_BUTTON_SIZE, move=(260, 440), style=GameConstants.BUTTON_STYLE_NOT_TOGGLED, hide=True)
        self.LevelButtonHard = self._make_button("Hard", size=GameConstants.LEVEL_BUTTON_SIZE, move=(440, 440), style=GameConstants.BUTTON_STYLE_NOT_TOGGLED, hide=True)
        self.LevelButtonEasy.clicked.connect(lambda : self.choose_difficulty("easy"))
        self.LevelButtonHard.clicked.connect(lambda : self.choose_difficulty("hard"))
        self.ImageResolutionLabel = self._make_label("Image resolution", size=(180, 80), move=(300, 240), style="font-size: 24px;color:black", hide= True, align=True)
        self.ImageResolution_8 = self._make_button("8x8", size=(80,80), move=(250, 320), style=GameConstants.CHANGE_RES_BUTTON_NOT_TOGGLED, hide=True)
        self.ImageResolution_8.clicked.connect( lambda : self.set_snapshot_resolution(8))
        self.ImageResolution_12 = self._make_button("12x12", size=(80,80), move=(350, 320), style=GameConstants.CHANGE_RES_BUTTON_NOT_TOGGLED, hide=True)
        self.ImageResolution_12.clicked.connect( lambda : self.set_snapshot_resolution(12))
        self.ImageResolution_16 = self._make_button("16x16", size=(80,80), move=(450, 320), style=GameConstants.CHANGE_RES_BUTTON_NOT_TOGGLED, hide=True)
        self.ImageResolution_16.clicked.connect( lambda : self.set_snapshot_resolution(16))

        # Game
        self.EndGameButton = self._make_button("End", size=GameConstants.GAME_BUTTON_SIZE, move=(300,530), style=GameConstants.GAME_BUTTON_STYLE, hide=True)
        self.EndGameButton.clicked.connect(self.end_game)
        self.StrategiesButton = self._make_button("Strategies", size=GameConstants.GAME_BUTTON_SIZE, move=(300,555), style=GameConstants.GAME_BUTTON_STYLE, hide=True)
        self.StrategiesButton.clicked.connect(self.write_strategies)
        self.EndTrainingButton = self._make_button("Stop the training", size=(200,20), move=(300, 580), style=GameConstants.GAME_BUTTON_STYLE, hide=True)
        self.EndTrainingButton.clicked.connect(self.end_training_mode)
        self.DeviationModeButton = self._make_button("Deviations", size=(80,40), move=(715,5), style="font-size: 15px;background: white; color:black", hide=True)
        self.DeviationModeButton.setCheckable(True)
        self.DeviationModeButton.clicked.connect(self.change_aspect)
        self.ShowDistributionButton = self._make_button("String-pattern\ndistribution", size=(80,40), move=(715, 60), style="font-size: 12px; background:white; color:black", hide=True)
        self.ShowDistributionButton.clicked.connect(self.show_barchart)
        self.GameScoreLabel = self._make_label(move=(0,25), style="font-size: 15px; color:black", hide=True)
        self.GameTurnsLabel = self._make_label("Turn: "+str(self.turns), move=(0,0), style="color:black;", hide=True)      
        self.GameCorrectLabel = self._make_label("Correct!", size=GameConstants.GAME_FEEDBACK_SIZE, move=(300,435), 
                                                style="font-size: 24px;background: rgb(64, 128, 2); color:black;", hide=True, align=True)
        self.GameWrongLabel = self._make_label("Wrong...", size=GameConstants.GAME_FEEDBACK_SIZE, move=(300,435), 
                                                style="font-size: 24px;background: rgb(204, 0, 0); color:black;", hide=True, align=True)
        self.TrainingEndedPopup = self._make_label("Training mode ended!", size=(450,250), move=(170, 150), 
                                                    style="font-size: 20px; border:1px solid black; color:black;", hide=True, align=True)
        self.LeftAnswerButton = self._make_button(None, size=GameConstants.ANSWER_BUTTON_SIZE, move=(50,435), style=GameConstants.ANSWER_BUTTON_STYLE, hide=True)
        self.MiddleAnswerButton = self._make_button(None, size=GameConstants.ANSWER_BUTTON_SIZE, move=(300,435), style=GameConstants.ANSWER_BUTTON_STYLE, hide=True)
        self.RightAnswerButton = self._make_button(None, size=GameConstants.ANSWER_BUTTON_SIZE, move=(550,435), style=GameConstants.ANSWER_BUTTON_STYLE, hide=True)
        #  Classic and Classic+ mode buttons
        self.AnswerButtonPiFlux = self._make_button("Pi-Flux", size=GameConstants.ANSWER_BUTTON_SIZE, move=(50,435), style=GameConstants.ANSWER_BUTTON_STYLE, hide=True)
        self.AnswerButtonStrings = self._make_button("Strings", size=GameConstants.ANSWER_BUTTON_SIZE, move=(550,435), style=GameConstants.ANSWER_BUTTON_STYLE, hide=True)        
        self.AnswerButtonSprinkled = self._make_button("Sprinkled holes", size=GameConstants.ANSWER_BUTTON_SIZE, move=(300,435), style=GameConstants.ANSWER_BUTTON_STYLE, hide=True)

        # Example Mode
        self.PifluxEasyButton = self._make_button("Pi-Flux\n(easy)", size=GameConstants.THEORY_BUTTON_SIZE, move=(5,460), style=GameConstants.THEORY_BUTTON_STYLE, hide=True)
        self.PifluxHardButton = self._make_button("Pi-Flux\n(hard)", size=GameConstants.THEORY_BUTTON_SIZE, move=(5,525), style=GameConstants.THEORY_BUTTON_STYLE, hide=True)
        self.StringsEasybutton = self._make_button("Strings\n(easy)", size=GameConstants.THEORY_BUTTON_SIZE, move=(110, 460), style=GameConstants.THEORY_BUTTON_STYLE, hide=True)
        self.StringsHardbutton = self._make_button("Strings\n(hard)", size=GameConstants.THEORY_BUTTON_SIZE, move=(110, 525), style=GameConstants.THEORY_BUTTON_STYLE, hide=True)
        self.SprinkledButton = self._make_button("Sprinkled\nholes", size=GameConstants.THEORY_BUTTON_SIZE, move=(215,460), style=GameConstants.THEORY_BUTTON_STYLE, hide=True)
        self.T1Button = self._make_button("T = 0.1 J", size=GameConstants.THEORY_BUTTON_SIZE, move=(320,460), style=GameConstants.THEORY_BUTTON_STYLE, hide=True)
        self.T8Button = self._make_button("T = 0.8 J", size=GameConstants.THEORY_BUTTON_SIZE, move=(320,525), style=GameConstants.THEORY_BUTTON_STYLE, hide=True)
        self.ReturnToMainMenu = self._make_button("Main menu", size=(200,30), move=(300,530), style="font-size: 24px;background: white; color:black;", hide=True)
        self.ReturnToMainMenu.clicked.connect(self.go_to_main_screen)
        self.Popup = self._make_label("", size=(550,150), move=(120,150),
                                                style="font-size: 20px; border:1px solid black; color:black;", hide=True, align=True)
        # Example mode images
        self.ExampleModeRegImageLeft = self._make_image()
        self.ExampleModeDevImageLeft = self._make_image()
        self.ExampleModeRegImageRight = self._make_image()
        self.ExampleModeDevImageRight = self._make_image()
        self.ExampleModeLeftTheoryLabel = self._make_label("No theory", size=(200,25), move=(125, 8), 
                                                            style="font-size: 20px; color: black;", hide=True)
        self.ExampleModeRightTheoryLabel = self._make_label("No theory", size=(200,25), move=(525, 8), 
                                                             style="font-size: 20px; color: black;", hide=True)
        # More buttons specifically for examples mode
        self.Images1_button = self._make_button("1", size=(45,45), move=(460, 490), style="font-size: 18px; background: white; color: black;", hide=True)
        self.Images1_button.clicked.connect(lambda : self.set_number_of_example_images(1))
        self.Images4_button = self._make_button("4", size=(45,45), move=(510, 490), style="font-size: 18px; background: white; color: black;", hide=True)
        self.Images4_button.clicked.connect(lambda : self.set_number_of_example_images(4))
        self.Images16_button = self._make_button("16", size=(45,45), move=(460, 540), style="font-size: 18px; background: white; color: black;", hide=True)
        self.Images16_button.clicked.connect(lambda : self.set_number_of_example_images(16))
        self.Images25_button = self._make_button("25", size=(45,45), move=(510, 540), style="font-size: 18px; background: white; color: black;", hide=True)
        self.Images25_button.clicked.connect(lambda : self.set_number_of_example_images(25))
        self.ExampleModeImagesLabel = self._make_label("# of examples", size=(130, 25), move=(450, 450),
                                                        style="font-size: 20px; color: black;", hide=True, align=True)

        # Comparison mode
        self.CompModeButtonPiStrings = self._make_button("Pi-Flux | Strings", size=GameConstants.COMPARISON_BUTTON_SIZE, move=(420,435), 
                                                        style=GameConstants.COMPARISON_BUTTON_STYLE, hide=True)
        self.CompModeButtonStringsPi = self._make_button("Strings | Pi-Flux", size=GameConstants.COMPARISON_BUTTON_SIZE, move=(160,435),
                                                        style=GameConstants.COMPARISON_BUTTON_STYLE, hide=True)

        # drawing mode
        self.centralwidget.hide()
        self.DrawTheoryLabel = self._make_label("Try to draw your own Pi-flux picture!", size=GameConstants.DRAW_MODE_LABEL_SIZE, 
                                                move=GameConstants.DRAW_MODE_LABEL_MOVE, style=GameConstants.DRAW_MODE_LABEL_STYLE, hide=True, align=True)
        self.DrawStringsPopup = self._make_label("Good job! Now draw a strings picture!", size=GameConstants.EXAMPLE_MODE_POPUP_SIZE, 
                                                move=GameConstants.EXAMPLE_MODE_POPUP_MOVE, style=GameConstants.EXAMPLE_MODE_POPUP_STYLE, hide=True, align=True)
        self.FinishDrawingButton = self._make_button("Finish", size=GameConstants.DRAW_MODE_BUTTON_SIZE, move=(660, 100), 
                                                        style=GameConstants.DRAW_MODE_BUTTON_STYLE, hide=True)
        self.FinishDrawingButton.clicked.connect(self.finish_drawing)
        self.ResetDrawingButton = self._make_button("Reset", size=GameConstants.DRAW_MODE_BUTTON_SIZE, move=(660, 200),
                                                    style=GameConstants.DRAW_MODE_BUTTON_STYLE, hide=True)
        self.ResetDrawingButton.clicked.connect(self.reset_drawing)

        # Buttons cycle through colors when clicked in Alt. mode
        self.button_0_0.clicked.connect(lambda : self.change_button_color(self.button_0_0))
        self.button_0_1.clicked.connect(lambda : self.change_button_color(self.button_0_1))
        self.button_0_2.clicked.connect(lambda : self.change_button_color(self.button_0_2))
        self.button_0_3.clicked.connect(lambda : self.change_button_color(self.button_0_3))
        self.button_0_4.clicked.connect(lambda : self.change_button_color(self.button_0_4))
        self.button_0_5.clicked.connect(lambda : self.change_button_color(self.button_0_5))
        self.button_1_0.clicked.connect(lambda : self.change_button_color(self.button_1_0))
        self.button_1_1.clicked.connect(lambda : self.change_button_color(self.button_1_1))
        self.button_1_2.clicked.connect(lambda : self.change_button_color(self.button_1_2))
        self.button_1_3.clicked.connect(lambda : self.change_button_color(self.button_1_3))
        self.button_1_4.clicked.connect(lambda : self.change_button_color(self.button_1_4))
        self.button_1_5.clicked.connect(lambda : self.change_button_color(self.button_1_5))
        self.button_2_0.clicked.connect(lambda : self.change_button_color(self.button_2_0))
        self.button_2_1.clicked.connect(lambda : self.change_button_color(self.button_2_1))        
        self.button_2_2.clicked.connect(lambda : self.change_button_color(self.button_2_2))
        self.button_2_3.clicked.connect(lambda : self.change_button_color(self.button_2_3))
        self.button_2_4.clicked.connect(lambda : self.change_button_color(self.button_2_4))
        self.button_2_5.clicked.connect(lambda : self.change_button_color(self.button_2_5))
        self.button_3_0.clicked.connect(lambda : self.change_button_color(self.button_3_0))
        self.button_3_1.clicked.connect(lambda : self.change_button_color(self.button_3_1))
        self.button_3_2.clicked.connect(lambda : self.change_button_color(self.button_3_2))
        self.button_3_3.clicked.connect(lambda : self.change_button_color(self.button_3_3))
        self.button_3_4.clicked.connect(lambda : self.change_button_color(self.button_3_4))
        self.button_3_5.clicked.connect(lambda : self.change_button_color(self.button_3_5))
        self.button_4_0.clicked.connect(lambda : self.change_button_color(self.button_4_0))
        self.button_4_1.clicked.connect(lambda : self.change_button_color(self.button_4_1))
        self.button_4_2.clicked.connect(lambda : self.change_button_color(self.button_4_2))
        self.button_4_3.clicked.connect(lambda : self.change_button_color(self.button_4_3))
        self.button_4_4.clicked.connect(lambda : self.change_button_color(self.button_4_4))
        self.button_4_5.clicked.connect(lambda : self.change_button_color(self.button_4_5))
        self.button_5_0.clicked.connect(lambda : self.change_button_color(self.button_5_0))
        self.button_5_1.clicked.connect(lambda : self.change_button_color(self.button_5_1))
        self.button_5_2.clicked.connect(lambda : self.change_button_color(self.button_5_2))
        self.button_5_3.clicked.connect(lambda : self.change_button_color(self.button_5_3))
        self.button_5_4.clicked.connect(lambda : self.change_button_color(self.button_5_4))
        self.button_5_5.clicked.connect(lambda : self.change_button_color(self.button_5_5))


        # Strategies Screen
        self.StrategiesBackground = self._make_label(   size=(400,550), move=(200,20), 
                                                        style="font-size: 24px;background: rgb(51, 102, 153); border:1px solid black; color:black;",
                                                        hide=True)
        self.StrategiesLabel = self._make_label(   "Strategies", size=(100,50), move=(350,30), 
                                                    style="font-size: 21px;background: white; color:black;", hide=True,  align=True)
        self.StrategiesInput = QPlainTextEdit(self)
        self.StrategiesInput.resize(300,420)
        self.StrategiesInput.move(250, 95)  
        self.StrategiesInput.setStyleSheet("font-size: 18px;background: white; color:black;")
        self.StrategiesInput.hide()
        self.StrategiesFinishButton = self._make_button("Finish", size=(60,30), move=(520,525), style="font-size: 18px;background: white; color:black;", hide=True)
                
        # Snapshots
        self.RegularImage = self._make_image()
        self.DeviationImage = self._make_image()
        # Snapshots for comp mode
        self.LeftRegularImage = self._make_image()
        self.LeftDeviationImage = self._make_image()
        self.RightRegularImage = self._make_image()
        self.RightDeviationImage = self._make_image()
        # Strings distribution
        self.HistogramImage = self._make_image()

        # Show login screen
        self.setup_login_screen()


    """Aux constructor methods"""
    def _make_button(self, text, size=None, move=None, style=None, hide=False):
        # type (text : str) -> QPushbutton
        """ Make a new button and return it """
        if text is not None:
            Button = QPushButton(text, parent=self) # Create new button with text on
        else:
            Button = QPushButton(self)
        # Additional options
        if size is not None:
            Button.resize(*size) # Unpack tuple to match resize args
        if move is not None:
            Button.move(*move)  
        if style is not None:
            Button.setStyleSheet(style)
        if hide:
            Button.hide()

        return Button

    def _make_label(self, text=None, size=None, move=None, style=None, hide=False, align=False):
        # type () -> QLabel
        """ Make a Label and return it"""
        if text is not None:
            Label = QLabel(text, self)
        else:
            Label = QLabel(self)

        if size is not None:
            Label.resize(*size)
        if move is not None:
            Label.move(*move)
        if style is not None:   
            Label.setStyleSheet(style)
        if hide:
            Label.hide()
        if align:
            Label.setAlignment(QtCore.Qt.AlignCenter)
        return Label

    def _make_lineEdit(self, size=None, move=None, style=None):
        # type () -> QLineEdit
        lineEdit = QLineEdit(self)
        if size is not None:
            lineEdit.resize(*size)
        if move is not None:
            lineEdit.move(*move)
        if style is not None:
            lineEdit.setStyleSheet(style)
        return lineEdit
    
    def _make_image(self):
        # type () -> QLabel
        Image = QLabel(self)
        Image.hide()
        Image.setScaledContents(True)
        return Image

    """ Other methods """

    def setup_login_screen(self) -> None:
        """ Sets up login screen based on if there are saved results or not """
        if self.DataContainer.check_for_results():
            self.LoadSaveGameLabel.show()
            self.SavedGameButton1.show()
            self.SavedGameButton2.show()
            self.SavedGameButton3.show()
            self.NewLoginButton.show()

            names = self.DataContainer.get_old_session_names()
            while len(names) < 3:
                names.append(None)
            names = names[:3]
            self.SavedGameButton1.setText(names[0])
            self.SavedGameButton1.clicked.connect(lambda : self.load_saved_session(names[0]))
            if names[1] is not None:
                self.SavedGameButton2.setText(names[1])
                self.SavedGameButton2.clicked.connect(lambda : self.load_saved_session(names[1]))
                if names[2] is not None:
                    self.SavedGameButton3.setText(names[2])
                    self.SavedGameButton3.clicked.connect(lambda : self.load_saved_session(names[2]))
                else:
                    self.SavedGameButton3.setEnabled(False)
                    self.SavedGameButton3.setStyleSheet(GameConstants.LOAD_GAME_BUTTON_STYLE_NOT_ENABLED)
            else:
                self.SavedGameButton2.setEnabled(False)
                self.SavedGameButton2.setStyleSheet(GameConstants.LOAD_GAME_BUTTON_STYLE_NOT_ENABLED)
                self.SavedGameButton3.setEnabled(False)
                self.SavedGameButton3.setStyleSheet(GameConstants.LOAD_GAME_BUTTON_STYLE_NOT_ENABLED)
        else:
            self.show_login_screen()

    def show_login_screen(self) -> None:
        """ Shows the login screen """
        # Hide load game screen
        self.LoadSaveGameLabel.hide()
        self.SavedGameButton1.hide()
        self.SavedGameButton2.hide()
        self.SavedGameButton3.hide()
        self.NewLoginButton.hide()

        # Show login screen
        self.NameInput.show()
        self.LoginNameLabel.show()
        self.LoginPhysicistLabel.show()
        self.PhysicistButtonYes.show()
        self.PhysicistButtonNo.show()
        self.PhysicistLevelLabel.show()
        self.PhysicistButtonGeneral.show()
        self.PhysicistButtonTheorist.show()
        self.PhysicistButtonExperimentalist.show()
        self.ChooseLevelLabel.show()
        self.LevelButtonEasy.show()
        self.LevelButtonHard.show()
        self.EndUserButton.show()

    def load_saved_session(self, name : str) -> None:
        """ Loads the saved data with the given name """
        # Load results
        self.DataContainer.load_previous_result(name)
        # Hide login screen
        self.LoadSaveGameLabel.hide()
        self.SavedGameButton1.hide()
        self.SavedGameButton2.hide()
        self.SavedGameButton3.hide()
        self.NewLoginButton.hide()
        # Raise options screen
        self.choose_difficulty("easy")
        self.set_snapshot_resolution(8)
        self.LevelButtonEasy.move(220, 160)
        self.LevelButtonHard.move(420, 160)
        self.ChooseLevelLabel.setText("Difficulty:")
        self.ChooseLevelLabel.move(300, 80)
        self.toggle_options_screen()
        self.OptionsButton.show()

    def save_login_info(self):
        # type () -> None
        """ Save login info, hide login buttons, show home screen """
        if self.NameInput.text():
            self.DataContainer.player_name = self.NameInput.text()
        self.PhysicistButtonYes.hide()
        self.PhysicistButtonNo.hide()
        
        self.LoginNameLabel.hide()
        self.NameInput.hide()
        self.LoginPhysicistLabel.hide()
        self.EndUserButton.hide()
        if not self.doping:
            self.choose_difficulty("easy")
        self.set_snapshot_resolution(8)
        self.PhysicistLevelLabel.hide()
        self.PhysicistButtonGeneral.hide()
        self.PhysicistButtonTheorist.hide()
        self.PhysicistButtonExperimentalist.hide()
        
        # Present home screen
        self.ModeButtonClassic.show()
        self.ModeButtonDrawing.show()
        self.ChooseModeLabel.show()
        self.ModeButtonComparison.show()
        self.ModeButtonHeisenberg.show()
        self.ModeButtonExamples.show()
        self.ModeButtonSprinkled.show()
        self.OptionsButton.show()
        # Move these for the options screen later
        self.LevelButtonEasy.move(220, 160)
        self.LevelButtonEasy.hide()
        self.LevelButtonHard.move(420, 160)
        self.LevelButtonHard.hide()
        self.ChooseLevelLabel.setText("Difficulty:")
        self.ChooseLevelLabel.move(300, 80)
        self.ChooseLevelLabel.hide()

    def toggle_options_screen(self):
        # type () -> None
        """ Raises / lowers option buttons """
        self.option_screen = not self.option_screen

        if self.option_screen:
            self.ChooseModeLabel.hide()

            self.ModeButtonSprinkled.hide()
            self.ModeButtonDrawing.hide()
            self.ModeButtonClassic.hide()
            self.ModeButtonExamples.hide()
            self.ModeButtonHeisenberg.hide()
            self.ModeButtonComparison.hide()

            self.OptionsScreenLabel.show()
            self.ChooseLevelLabel.show()
            self.LevelButtonEasy.show()
            self.LevelButtonHard.show()
            self.ImageResolutionLabel.show()
            self.ImageResolution_8.show()
            self.ImageResolution_12.show()
            self.ImageResolution_16.show()
            self.OptionsButton.setText("Return")
            
        else:
            self.ChooseModeLabel.show()
            self.ModeButtonSprinkled.show()
            self.ModeButtonDrawing.show()
            self.ModeButtonClassic.show()
            self.ModeButtonExamples.show()
            self.ModeButtonHeisenberg.show()
            self.ModeButtonComparison.show()

            self.OptionsScreenLabel.hide()
            self.ChooseLevelLabel.hide()
            self.LevelButtonEasy.hide()
            self.LevelButtonHard.hide()
            self.ImageResolutionLabel.hide()
            self.ImageResolution_8.hide()
            self.ImageResolution_12.hide()
            self.ImageResolution_16.hide()
            self.OptionsButton.setText("Options")

    def set_player_is_physicist(self, answer):
        # type (answer : bool) -> None
        """ Set player is physicist, enable/disable buttons based on answer """
        self.DataContainer.player_is_physicist = answer
        self.PhysicistButtonGeneral.setEnabled(answer)
        self.PhysicistButtonTheorist.setEnabled(answer)
        self.PhysicistButtonExperimentalist.setEnabled(answer)
        self.PhysicistButtonYes.setEnabled(not answer)
        self.PhysicistButtonNo.setEnabled(answer)
        color = "white;" if answer else "grey;"
        self.PhysicistButtonGeneral.setStyleSheet("font-size: 20px; color:black; background: "+color)
        self.PhysicistButtonTheorist.setStyleSheet("font-size: 18px; color:black; background: "+color)
        self.PhysicistButtonExperimentalist.setStyleSheet("font-size: 18px; color:black; background: "+color)
        if answer: # Yes
            self.PhysicistButtonYes.setStyleSheet(GameConstants.BUTTON_STYLE_TOGGLED)
            self.PhysicistButtonNo.setStyleSheet(GameConstants.BUTTON_STYLE_NOT_TOGGLED)
        else: # No
            self.PhysicistButtonYes.setStyleSheet(GameConstants.BUTTON_STYLE_NOT_TOGGLED)
            self.PhysicistButtonNo.setStyleSheet(GameConstants.BUTTON_STYLE_TOGGLED)
            self.set_player_physicist_level(None)

    def set_player_physicist_level(self, answer):
        # type (answer : str or None) -> None
        """ Set the player physicist level based on reported answer """
        self.DataContainer.player_physicist_level = answer
        if answer == "general":
            self.PhysicistButtonGeneral.setStyleSheet("font-size: 20px; color:black; background: #C0C0C0")
            self.PhysicistButtonTheorist.setStyleSheet("font-size: 18px; color:black; background: white")
            self.PhysicistButtonExperimentalist.setStyleSheet("font-size: 18px; color:black; background: white")
        elif answer == "theorist":
            self.PhysicistButtonGeneral.setStyleSheet("font-size: 20px; color:black; background: white")
            self.PhysicistButtonTheorist.setStyleSheet("font-size: 18px; color:black; background: #C0C0C0")
            self.PhysicistButtonExperimentalist.setStyleSheet("font-size: 18px; color:black; background: white")
        elif answer == "experimentalist":
            self.PhysicistButtonGeneral.setStyleSheet("font-size: 20px; color:black; background: white")
            self.PhysicistButtonTheorist.setStyleSheet("font-size: 18px; color:black; background: white")
            self.PhysicistButtonExperimentalist.setStyleSheet("font-size: 18px; color:black; background: #C0C0C0")

    def choose_difficulty(self, choice):
        # type (choice : str) -> None
        """ Set doping level """
        if choice == "hard":
            self.doping = GameConstants.DOPING_LEVELS[1]
            self.ModeButtonSprinkled.setEnabled(True)
            self.ModeButtonSprinkled.setStyleSheet(GameConstants.BUTTON_STYLE_NOT_TOGGLED)
            self.LevelButtonHard.setDown(True)
            self.LevelButtonHard.setStyleSheet(GameConstants.BUTTON_STYLE_TOGGLED)
            self.LevelButtonEasy.setDown(False)
            self.LevelButtonEasy.setStyleSheet(GameConstants.BUTTON_STYLE_NOT_TOGGLED)
        else:
            self.doping = GameConstants.DOPING_LEVELS[0]
            self.ModeButtonSprinkled.setEnabled(False)
            self.ModeButtonSprinkled.setStyleSheet(GameConstants.BUTTON_STYLE_TOGGLED)
            self.LevelButtonHard.setDown(False)
            self.LevelButtonHard.setStyleSheet(GameConstants.BUTTON_STYLE_NOT_TOGGLED)
            self.LevelButtonEasy.setDown(True)   
            self.LevelButtonEasy.setStyleSheet(GameConstants.BUTTON_STYLE_TOGGLED)
        
    def set_snapshot_resolution(self, dim):
        # type (dim : int) -> None
        """ Sets the dimensions that generated snapshots are cropped to. Max is 16x16 """
        self.ImageGenerator.set_cropping_dimension(dim)
        # Color buttons
        button_dict = {8 : self.ImageResolution_8, 12: self.ImageResolution_12, 16 : self.ImageResolution_16}
        for value in button_dict.keys():
            button_dict[value].setStyleSheet(GameConstants.CHANGE_RES_BUTTON_NOT_TOGGLED)
        
        button_dict[dim].setStyleSheet(GameConstants.CHANGE_RES_BUTTON_TOGGLED)

    def setup_gamemode(self, game_mode):
        # type (game_mode : str) -> None
        """ Set up a game in the chosen game mode """
        # Hide home screen
        self.ModeButtonClassic.hide()
        self.ModeButtonDrawing.hide()
        self.ModeButtonSprinkled.hide()
        self.ModeButtonComparison.hide()
        self.ModeButtonHeisenberg.hide()
        self.ModeButtonExamples.hide()
        self.ChooseModeLabel.hide()
        self.OptionsButton.hide()

        self.current_game_mode = game_mode
        # Show game buttons
        if game_mode == GameConstants.GAME_MODE_COMPARISON: 
            self.single_image_mode = False
            self.chooseable_theories = [0, 1] # flux, strings
            self.GameCorrectLabel.resize(220,72)
            self.GameCorrectLabel.move(295, 520)
            self.GameWrongLabel.resize(220, 72)
            self.GameWrongLabel.move(295,520)
            self.EndGameButton.resize(200,30)
            self.EndGameButton.move(590, 520)
            self.EndGameButton.setStyleSheet("""font-size: 24px;background: white;""")
            self.StrategiesButton.resize(200,30)
            self.StrategiesButton.move(590, 555)
            self.StrategiesButton.setStyleSheet("""font-size: 24px;background: white;""")
            self.ReturnToMainMenu.resize(200, 30)
            self.ReturnToMainMenu.move(5, 520)
            self.EndTrainingButton.resize(200,30)
            self.EndTrainingButton.move(5, 555)
            self.EndTrainingButton.setStyleSheet("""font-size: 24px;background: white;""")

            self.CompModeButtonPiStrings.show()
            self.CompModeButtonPiStrings.clicked.connect(lambda : self.evaluate_answer( 0 )) # Left theory is PiFlux
            self.CompModeButtonStringsPi.show()
            self.CompModeButtonStringsPi.clicked.connect(lambda : self.evaluate_answer( 1 )) # Left theory is strings
        
        elif game_mode in [GameConstants.GAME_MODE_EXAMPLES, GameConstants.GAME_MODE_DRAWING]:
            self.ReturnToMainMenu.show()
            self.chooseable_theories = GameConstants.GAME_MODE_DICT[game_mode]
            self.DeviationModeButton.show()

            self.PifluxEasyButton.show()
            self.StringsEasybutton.show()
            self.SprinkledButton.show()
            self.T1Button.show()
            self.T8Button.show()
            self.ReturnToMainMenu.move(590,560)

            if game_mode == GameConstants.GAME_MODE_EXAMPLES:
                self.ExampleModeRightTheoryLabel.show()
                self.ExampleModeLeftTheoryLabel.show()
                self.ExampleModeImagesLabel.show()
                self.PifluxHardButton.show()
                self.StringsHardbutton.show()
                self.Images1_button.show()
                self.Images4_button.show()
                self.Images16_button.show()
                self.Images25_button.show()
                self.DeviationModeButton.move(590, 520)
                self.DeviationModeButton.resize(200,30)
                self.DeviationModeButton.setStyleSheet("font-size: 24px;background: white; color:black")
                self.PifluxEasyButton.setText("Pi-flux\n(easy)")
                self.StringsEasybutton.setText("Strings\n(easy)")
                self.Popup.setText("Click on the theory that you want to see.\nNote that loading many images may take some time.")
                self.PifluxEasyButton.clicked.connect(lambda : self.show_example_images(0, 4.5, "Pi-flux, easy"))
                self.PifluxHardButton.clicked.connect(lambda : self.show_example_images(0, 9.0, "Pi-flux, hard"))
                self.StringsEasybutton.clicked.connect(lambda : self.show_example_images(1, 4.5, "Strings, easy"))
                self.StringsHardbutton.clicked.connect(lambda : self.show_example_images(1, 9.0, "Strings, hard"))
                self.SprinkledButton.clicked.connect(lambda : self.show_example_images(2, 0, "Sprinkled holes, hard"))
                self.T1Button.clicked.connect(lambda : self.show_example_images(3, 0, "T = 0.1 J"))
                self.T8Button.clicked.connect(lambda : self.show_example_images(4, 0, "T = 0.8 J"))
                self.Images1_button.click()
                if self.deviation_mode:
                    self.ExampleModeDevImageLeft.show()
                    self.ExampleModeDevImageRight.show()
                else:
                    self.ExampleModeRegImageLeft.show()
                    self.ExampleModeRegImageRight.show()

            else:
                self.Popup.setText("Click on the theory that you want to draw.")
                self.PifluxEasyButton.setText("Pi-flux")
                self.StringsEasybutton.setText("Strings")
                self.PifluxEasyButton.clicked.connect(lambda : self.show_drawing_board(0))
                self.StringsEasybutton.clicked.connect(lambda : self.show_drawing_board(1))
                self.SprinkledButton.clicked.connect(lambda : self.show_drawing_board(2))
                self.T1Button.clicked.connect(lambda : self.show_drawing_board(3))
                self.T8Button.clicked.connect(lambda : self.show_drawing_board(4))
            
            self.Popup.show()
            return None
            
        else: # Classic, Random, Heisenberg mode, alternative mode
            self.ReturnToMainMenu.resize(200, 30)
            self.ReturnToMainMenu.move(5, 550)
            self.EndTrainingButton.resize(200,30)
            self.EndTrainingButton.move(5, 510)
            self.chooseable_theories = GameConstants.GAME_MODE_DICT[game_mode]
            self.LeftAnswerButton.setText(GameConstants.THEORYDICT[ self.chooseable_theories[0] ])
            self.LeftAnswerButton.clicked.connect(lambda : self.evaluate_answer( self.chooseable_theories[0] ))
            self.LeftAnswerButton.show()
            self.RightAnswerButton.setText(GameConstants.THEORYDICT[ self.chooseable_theories[1] ])
            self.RightAnswerButton.clicked.connect(lambda : self.evaluate_answer( self.chooseable_theories[1] ))
            self.RightAnswerButton.show()

            if game_mode == GameConstants.GAME_MODE_CLASSICPLUS:
                self.highligt_answer = True
                self.MiddleAnswerButton.setText(GameConstants.THEORYDICT[ self.chooseable_theories[2] ])
                self.MiddleAnswerButton.clicked.connect(lambda : self.evaluate_answer( self.chooseable_theories[2] ))
                self.MiddleAnswerButton.show()
                # Make space for 3 answer buttons
                self.GameCorrectLabel.move(300,515)
                self.GameWrongLabel.move(300,515)
                self.StrategiesButton.move(590,515)
                self.EndGameButton.move(590,565)

        # Show game buttons
        self.DeviationModeButton.show()
        self.training_mode = True
        self.EndGameButton.show()
        self.StrategiesButton.show()
        self.EndTrainingButton.show()

        # Generate first snapshot
        n = random.randint(0, len(self.chooseable_theories) - 1)
        if self.single_image_mode:
            self.show_theory_image(self.chooseable_theories[n])
        else: 
            # In this case n is the theory on the left (for evaluation purposes)
            theory1 = self.chooseable_theories[n]
            theory2 = (theory1 + 1) % 2
            self.show_theory_images(theory1, theory2)
       
    def show_theory_image(self, theory: int, doping=0.0) -> None:
        """ Show a picture based on the theory chosen """
        self.current_theory = theory
        if doping == 0.0: # Doping can optionally be controlled
            doping = self.doping
        RegImg, DevImg = self.ImageGenerator.generate_images(self.current_theory, doping)
        self._update_snaphots(RegImg, DevImg)

        self.ImageGenerator.strings_distribution = {}
        if self.current_game_mode != GameConstants.GAME_MODE_HEISENBERG:
            self.ShowDistributionButton.show()

        if self.is_showing_histogram:
            self._update_barchart()
        elif self.deviation_mode:
            self.RegularImage.hide()
            self.DeviationImage.show()
        else:
            self.RegularImage.show()
            self.DeviationImage.hide()

    def show_theory_images(self, theory1, theory2):
        # type (theory1 : int, theory2 : int) -> None
        """ Update left and right images s.t. left has theory 1 and right has theory 2"""
        self.current_theory = theory1
        self.ShowDistributionButton.show()
        self.ImageGenerator.strings_distribution = {} # Reset string distr.

        for Reg,Dev,theory,rect in [ [self.LeftRegularImage, self.LeftDeviationImage,theory1,GameConstants.LEFT_IMAGE_RECTANGLE], 
                                [self.RightRegularImage, self.RightDeviationImage, theory2,GameConstants.RIGHT_IMAGE_RECTANGLE]]:
            RegImg, DevImg = self.ImageGenerator.generate_images(theory, self.doping)
            # Update regular snapshot
            qRegImg = ImageQt(RegImg)
            RegPixmap = QtGui.QPixmap.fromImage(qRegImg)
            Reg.setPixmap(RegPixmap)
            Reg.setScaledContents = True
            Reg.setGeometry(QtCore.QRect(*rect))
            #Update Deviation pic
            qDevImg = ImageQt(DevImg)
            DevPixmap = QtGui.QPixmap.fromImage(qDevImg)
            Dev.setPixmap(DevPixmap)
            Dev.setScaledContents = True
            Dev.setGeometry(QtCore.QRect(*rect))

        if self.is_showing_histogram:
            self._update_comp_mode_barcharts()
        elif self.deviation_mode:
            self.LeftDeviationImage.show()
            self.LeftRegularImage.hide()
            self.RightDeviationImage.show()
            self.RightRegularImage.hide()
        else:
            self.LeftDeviationImage.hide()
            self.LeftRegularImage.show()
            self.RightDeviationImage.hide()
            self.RightRegularImage.show()

    def show_example_images(self, theory : int, doping : float, label : str) -> None:
        """ 
        Generate a number of images, displayed in some kind of order 
        """
        # Generate image
        RegImg, DevImg = self.ImageGenerator.generate_example_image(theory, doping)
        # Insert into appropriate image objects
        if self.examples_are_left_side:
            self.ExampleModeLeftTheoryLabel.setText(label)
            # Update regular snapshot
            qRegImg = ImageQt(RegImg)
            RegPixmap = QtGui.QPixmap.fromImage(qRegImg)
            self.ExampleModeRegImageLeft.setPixmap(RegPixmap)
            self.ExampleModeRegImageLeft.setScaledContents = True
            self.ExampleModeRegImageLeft.setGeometry(QtCore.QRect(*GameConstants.LEFT_EXAMPLE_RECTANGLE))
            #Update Deviation pic
            qDevImg = ImageQt(DevImg)
            DevPixmap = QtGui.QPixmap.fromImage(qDevImg)
            self.ExampleModeDevImageLeft.setPixmap(DevPixmap)
            self.ExampleModeDevImageLeft.setScaledContents = True
            self.ExampleModeDevImageLeft.setGeometry(QtCore.QRect(*GameConstants.LEFT_EXAMPLE_RECTANGLE))
            if self.deviation_mode:
                self.ExampleModeRegImageLeft.hide()
                self.ExampleModeDevImageLeft.show()
            else:
                self.ExampleModeRegImageLeft.show()
                self.ExampleModeDevImageLeft.hide()
        else:
            self.ExampleModeRightTheoryLabel.setText(label)
            # Update regular snapshot
            qRegImg = ImageQt(RegImg)
            RegPixmap = QtGui.QPixmap.fromImage(qRegImg)
            self.ExampleModeRegImageRight.setPixmap(RegPixmap)
            self.ExampleModeRegImageRight.setScaledContents = True
            self.ExampleModeRegImageRight.setGeometry(QtCore.QRect(*GameConstants.RIGHT_EXAMPLE_RECTANGLE))
            #Update Deviation pic
            qDevImg = ImageQt(DevImg)
            DevPixmap = QtGui.QPixmap.fromImage(qDevImg)
            self.ExampleModeDevImageRight.setPixmap(DevPixmap)
            self.ExampleModeDevImageRight.setScaledContents = True
            self.ExampleModeDevImageRight.setGeometry(QtCore.QRect(*GameConstants.RIGHT_EXAMPLE_RECTANGLE))
            if self.deviation_mode:
                self.ExampleModeRegImageRight.hide()
                self.ExampleModeDevImageRight.show()
            else:
                self.ExampleModeRegImageRight.show()
                self.ExampleModeDevImageRight.hide()
        self.examples_are_left_side = not self.examples_are_left_side

    def set_number_of_example_images(self, n : int) -> None:
        self.ImageGenerator.number_of_example_images = n
        for Button in [self.Images1_button, self.Images4_button, self.Images16_button, self.Images25_button]:
            Button.setStyleSheet("font-size: 18px; background: white; color: black;")
        if n == 1:
            self.Images1_button.setStyleSheet("font-size: 18px; background: grey; color: black;")
        elif n == 4:
            self.Images4_button.setStyleSheet("font-size: 18px; background: grey; color: black;")
        elif n == 16:
            self.Images16_button.setStyleSheet("font-size: 18px; background: grey; color: black;")
        else:
            self.Images25_button.setStyleSheet("font-size: 18px; background: grey; color: black;")

    def show_barchart(self):
        # type () -> None
        """ Show barchart of analytical string length distribution of the current snapshot """
        if self.current_game_mode == GameConstants.GAME_MODE_COMPARISON:
            # In comparison mode, the snapshots are highlighted when the histogram is generated
            if not self.ImageGenerator.strings_distribution:
                self._update_comp_mode_barcharts()
            
            if self.is_showing_histogram:
                self.HistogramImage.hide()
            else:
                self.HistogramImage.show()

        else:
            if not self.ImageGenerator.strings_distribution:
                self._update_barchart()
            
            if self.is_showing_histogram:
                self.HistogramImage.hide()
                if self.deviation_mode:
                    self.DeviationImage.show()
                else:
                    self.RegularImage.show()
            else:
                self.RegularImage.hide()
                self.DeviationImage.hide()
                self.HistogramImage.show()

        self.is_showing_histogram = not self.is_showing_histogram

    def _update_barchart(self) -> None:
        """ Update barchart figure """
        self.ImageGenerator.find_string_patterns(self.current_theory, self.doping)
        self.ImageGenerator.make_string_pattern_barchart_Image()
        self.HistogramImage.setPixmap(QtGui.QPixmap("./results/distribution.png"))
        self.HistogramImage.setScaledContents = True
        self.HistogramImage.setGeometry(QtCore.QRect(*GameConstants.SINGLE_IMAGE_RECTANGLE))

    def _update_comp_mode_barcharts(self) -> None:
        """ 
        Update the barchart based on both figures on Comparison Mode 
        and highlight both figures at the same time
        """
        left_theory = self.current_theory
        right_theory = (left_theory + 1) % 2 # since piflux and strings have codes 0 and 1
        # Left image first
        self.ImageGenerator.find_string_patterns(left_theory, self.doping)
        left_distribution = self.ImageGenerator.strings_distribution
        # Right image
        self.ImageGenerator.find_string_patterns(right_theory, self.doping)
        right_distribution = self.ImageGenerator.strings_distribution
        # Make histogram figure
        self.ImageGenerator.make_string_pattern_barchart_compMode_image(left_distribution, right_distribution)
        self.HistogramImage.setPixmap(QtGui.QPixmap("./results/distribution.png"))
        self.HistogramImage.setScaledContents = True
        self.HistogramImage.setGeometry(QtCore.QRect(*GameConstants.SINGLE_IMAGE_RECTANGLE))

    def _update_snaphots(self, RegImg : Image, DevImg : Image) -> None:
        """ Update regular and deviation snapshots with new images """
        # Update regular snapshot
        qRegImg = ImageQt(RegImg)
        RegPixmap = QtGui.QPixmap.fromImage(qRegImg)
        self.RegularImage.setPixmap(RegPixmap)
        self.RegularImage.setScaledContents = True
        self.RegularImage.setGeometry(QtCore.QRect(*GameConstants.SINGLE_IMAGE_RECTANGLE))
        #Update Deviation pic
        qDevImg = ImageQt(DevImg)
        DevPixmap = QtGui.QPixmap.fromImage(qDevImg)
        self.DeviationImage.setPixmap(DevPixmap)
        self.DeviationImage.setScaledContents = True
        self.DeviationImage.setGeometry(QtCore.QRect(*GameConstants.SINGLE_IMAGE_RECTANGLE))

    def show_drawing_board(self, theory_chosen):
        # type (theory_chosen : int) -> None
        """ 
        Show drawing board of buttons, 
        a label with the chosen theory,
        devation mode, finish, reset, return buttons  
        """
        self.PifluxEasyButton.hide()
        self.StringsEasybutton.hide()
        self.SprinkledButton.hide()
        self.T1Button.hide()
        self.T8Button.hide()
        self.ReturnToMainMenu.hide()
        self.Popup.hide()


        self.current_theory = theory_chosen
        self.centralwidget.show()
        self.FinishDrawingButton.show()
        self.ResetDrawingButton.show()
        self.DrawTheoryLabel.setText("Draw a " + GameConstants.THEORYDICT[self.current_theory] + "-theory snapshot!")
        self.DrawTheoryLabel.show()
        pass

    def end_training_mode(self):
        # type () -> None
        """ End training mode, start collecting points or drawing """
        self.training_mode = False
        self.GameCorrectLabel.hide()
        self.GameWrongLabel.hide()
        
        self.turns = 0
        self.points = 0 
        self.GameTurnsLabel.show()
        self.GameScoreLabel.show()

        self.EndTrainingButton.hide()
        self.DataContainer.count_up_run(self.current_game_mode, self.doping)
        self.TrainingEndedPopup.show()
        self.TrainingEndedPopup.raise_()
        timer = threading.Timer(1.0, self.TrainingEndedPopup.hide) 
        timer.start() 
        
        if 2 in self.chooseable_theories: # Random mode only
            self.StrategiesButton.resize(200,30)
            self.EndGameButton.resize(200,30)
            self.EndGameButton.move(590,550)
        
    def go_to_main_screen(self): 
        # type () -> None
        """ Return to main screen, saving results, hiding buttons and displaying the main screen """
        self.DataContainer.save_results()
        if self.StrategiesInput.toPlainText():
            self.DataContainer.append_strategies(self.current_game_mode,self.doping, 
                                                 self.StrategiesInput.toPlainText(),
                                                 self.turns, self.points)
            self.StrategiesInput.clear()
        
        self.training_mode = True
        self.points = 0
        self.GameScoreLabel.setText("Points : 0")
        self.turns = 0
        self.GameTurnsLabel.setText("Turn : 0")
        self.current_game_mode = None
        self.chooseable_theories = None
        self.single_image_mode = True

        self.highligt_answer = False
        for i in range(3):
            self.change_background_of_button(i, "white")

        self.GameCorrectLabel.hide()
        self.GameWrongLabel.hide()
        self.Popup.hide()
        
        if self.is_showing_histogram:
            self.show_barchart()
        self.ShowDistributionButton.hide()
        self.RegularImage.hide()
        self.DeviationImage.hide()
        self.RightRegularImage.hide()
        self.RightDeviationImage.hide()
        self.LeftRegularImage.hide()
        self.LeftDeviationImage.hide()
        
        # Reset buttons
        for Button in [ self.PifluxHardButton, self.LeftAnswerButton, self.RightAnswerButton, self.MiddleAnswerButton, 
                        self.CompModeButtonStringsPi, self.CompModeButtonPiStrings, self.PifluxEasyButton,
                        self.StringsEasybutton, self.StringsHardbutton, self.SprinkledButton, self.T1Button, self.T8Button]:
            Button.hide()
            try:
                Button.clicked.disconnect()
            except TypeError:
                pass


        self.EndGameButton.hide()
        self.StrategiesButton.hide()
        self.GameTurnsLabel.hide()
        self.GameScoreLabel.hide()

        # Hide example mode buttons
        self.PifluxEasyButton.hide()
        self.PifluxHardButton.hide()
        self.StringsEasybutton.hide()
        self.StringsHardbutton.hide()
        self.SprinkledButton.hide()
        self.T1Button.hide()
        self.T8Button.hide()
        self.Images1_button.hide()
        self.Images4_button.hide()
        self.Images16_button.hide()
        self.Images25_button.hide()
        self.ExampleModeRegImageRight.hide()
        self.ExampleModeRegImageLeft.hide()
        self.ExampleModeDevImageRight.hide()
        self.ExampleModeDevImageLeft.hide()
        self.ExampleModeLeftTheoryLabel.hide()
        self.ExampleModeRightTheoryLabel.hide()
        self.ExampleModeImagesLabel.hide()
        
        self.ReturnToMainMenu.hide()
        self.EndTrainingButton.hide()
        
        self.DeviationModeButton.hide()
        
        # Show game mode buttons
        self.ModeButtonClassic.show()
        self.ModeButtonDrawing.show()
        self.ModeButtonSprinkled.show()
        self.ModeButtonComparison.show()
        self.ModeButtonHeisenberg.show()
        self.ModeButtonExamples.show()
        self.ChooseModeLabel.show()
        self.OptionsButton.show()
        
    def evaluate_answer(self, theory_chosen):
        # type (theory_chosen : int) -> None
        """ Check if answer is correct, award points, append result and pass next turn"""
        # Reset button colors
        if self.highligt_answer:
            for theory in self.chooseable_theories:
                self.change_background_of_button(theory, "white")
        # Correct answer
        if theory_chosen == self.current_theory:
            self.GameWrongLabel.hide()
            self.GameCorrectLabel.show()
            if not self.training_mode:
                self.points += 1
                
        else: #incorrect answer
            self.GameCorrectLabel.hide()
            self.GameWrongLabel.show()
            if self.highligt_answer:
                self.change_background_of_button(self.current_theory, "green")

        if not self.training_mode:
            self.DataContainer.append_result(self.current_game_mode, self.doping, theory_chosen, 
                                             self.current_theory, self.ImageGenerator.cropped_size)
        self.next_turn()
                
    def change_background_of_button(self, theory, color):
        # type (theory : int, color : str) -> None
        """ Color the button with the correct answer a certain color """
        style_string = "font-size: 24px;background: "+color+"; color:black;"
        if theory == 0: # left button
            self.LeftAnswerButton.setStyleSheet(style_string)
        elif theory == 1: # Right button
            self.RightAnswerButton.setStyleSheet(style_string)
        else:
            self.MiddleAnswerButton.setStyleSheet(style_string)

    def next_turn(self):
        # type () -> None
        """ Updates turn counter, chooses theory and updates image """
        if not self.training_mode:
            self.turns += 1
            self.GameTurnsLabel.setText("Turn : " + str(self.turns))
            self.GameScoreLabel.setText("Points : " + str(self.points))
            self.GameScoreLabel.move(0,22)
            self.GameScoreLabel.show()
            self.GameTurnsLabel.show()

        n = random.randint(0, len(self.chooseable_theories) - 1)
        if self.single_image_mode:
            self.show_theory_image(self.chooseable_theories[n])
        else: 
            # In this case n is the theory on the left (for evaluation purposes)
            theory1 = self.chooseable_theories[n]
            theory2 = (theory1 + 1) % 2
            self.show_theory_images(theory1, theory2)
        if self.is_showing_histogram:
            self._update_barchart
            
    def change_aspect(self) -> None:
        """ Flip between regular and deviation view """
        if self.current_game_mode == GameConstants.GAME_MODE_DRAWING:
            if self.deviation_mode:
                self.deviation_mode = not self.deviation_mode
                self.drawing_mode_colors = GameConstants.ALT_MODE_REGULAR_COLORS
            else: 
                self.deviation_mode = True
                self.drawing_mode_colors = GameConstants.ALT_MODE_DEVIATION_COLORS
            for (k,v) in self.button_dict.items(): 
                color = v[2] # Color is 0,1 or 2 meaning [hole 0, up +1, down -1] or [hole, same, different]
                if color == 0: # Skip holes
                    continue

                else:
                    afm_reference = (v[0] + v[1]) % 2  # Reference AFM at site, 0=up(+1), 1=down(-1)
                    if self.deviation_mode:
                        if color == afm_reference + 1: 
                            v[2] = 1
                            k.setStyleSheet("font-size: 24px;background: " + self.drawing_mode_colors[1] + ";")
                        else:
                            v[2] = 2
                            k.setStyleSheet("font-size: 24px;background: " + self.drawing_mode_colors[2] + ";")
                    else:
                        if color == 1:
                            v[2] = afm_reference + 1
                            k.setStyleSheet("font-size: 24px;background: " + self.drawing_mode_colors[afm_reference + 1] + ";")
                        else:
                            afm_reference += 1
                            v[2] = (afm_reference % 2) + 1
                            k.setStyleSheet("font-size: 24px;background: " + self.drawing_mode_colors[(afm_reference % 2) + 1] + ";")
        elif self.current_game_mode == GameConstants.GAME_MODE_EXAMPLES:
            if self.deviation_mode:
                self.ExampleModeRegImageRight.show()
                self.ExampleModeRegImageLeft.show()
                self.ExampleModeDevImageRight.hide()
                self.ExampleModeDevImageLeft.hide()
            else:
                self.ExampleModeRegImageRight.hide()
                self.ExampleModeRegImageLeft.hide()
                self.ExampleModeDevImageRight.show()
                self.ExampleModeDevImageLeft.show()
            self.deviation_mode = not self.deviation_mode
        elif self.single_image_mode:
            self.deviation_mode = not self.deviation_mode
            if self.is_showing_histogram:
                self.show_barchart()
            elif self.deviation_mode:
                self.DeviationImage.show()
                self.RegularImage.hide()
            else:
                self.RegularImage.show()
                self.DeviationImage.hide()         
        else: 
            self.deviation_mode = not self.deviation_mode

            if self.deviation_mode:
                self.LeftDeviationImage.show()
                self.LeftRegularImage.hide()
                self.RightDeviationImage.show()
                self.RightRegularImage.hide()
            else:
                self.LeftDeviationImage.hide()
                self.LeftRegularImage.show()
                self.RightDeviationImage.hide()
                self.RightRegularImage.show()
            
    def change_button_color(self, Button):
        # type (Button : QPushButton) -> None
        """ Change color of a button when clicked """
        v = self.button_dict[Button] # Get current color
        v[2] += 1
        v[2] = v[2]%3
        Button.setStyleSheet("font-size: 24px;background: " + self.drawing_mode_colors[v[2]] + ";")

    def finish_drawing(self):
        # type () -> None
        self.FinishDrawingButton.hide()
        self.ResetDrawingButton.hide()
        drawing = self.drawing_to_array()
        self.DataContainer.append_drawing(drawing, GameConstants.THEORYDICT[self.current_theory])
        self.reset_drawing()
        self.centralwidget.hide()
        self.DrawTheoryLabel.hide()

        self.setup_gamemode(GameConstants.GAME_MODE_DRAWING)

    def reset_drawing(self):
        # type () -> None
        was_dev_mode = self.deviation_mode
        if was_dev_mode:
            self.DeviationModeButton.click()
        
        for button in self.drawing_mode_buttons_list:
            color = self.button_dict[button][2]
            afm_color = self.buttons_for_reset[button][2]
            while color != afm_color:
                self.change_button_color(button)
                color = self.button_dict[button][2]
        
        if was_dev_mode:
            self.DeviationModeButton.click()

    def drawing_to_array(self):
        # type () -> None
        drawing = [0] * GameConstants.ALT_MODE_DRAWING_DIMENSIONS 
        for i in range(GameConstants.ALT_MODE_DRAWING_DIMENSIONS):
            drawing[i] = GameConstants.ALT_MODE_DRAWING_DIMENSIONS * [ 0 ]
        
        if self.deviation_mode:
            self.DeviationModeButton.click()
        
        for button in self.drawing_mode_buttons_list:
            v = self.button_dict[button]
            if v[2] == 2:
                value = -1
            else:
                value = v[2]
            drawing[v[0]][v[1]] = value

        return drawing
   
    def write_strategies(self, game_finished=False):
        # type () -> None
        """ Open strategies screen, ready for player input """
        self.StrategiesLabel.show()
        self.StrategiesInput.show()
        self.StrategiesBackground.show()
        self.StrategiesFinishButton.show()
        
        self.StrategiesBackground.raise_()
        self.StrategiesLabel.raise_()
        self.StrategiesInput.raise_()
        self.StrategiesFinishButton.raise_()
        self.StrategiesFinishButton.clicked.connect(lambda : self.hide_strategies(game_finished))
        
    def hide_strategies(self, game_finished=False):
        # type () -> None
        """ Hide strategy screen """
        self.StrategiesLabel.hide()
        self.StrategiesInput.hide()
        self.StrategiesBackground.hide()
        self.StrategiesFinishButton.hide()
        
        if game_finished:
            if self.StrategiesInput.toPlainText():
                self.DataContainer.append_strategies(self.current_game_mode,self.doping, 
                                                 self.StrategiesInput.toPlainText(),
                                                 self.turns, self.points)
            answer = QtWidgets.QMessageBox.information(self, "End", "Thank you! Would you like to return to the main menu?", QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
            if answer == QtWidgets.QMessageBox.Yes:
                self.go_to_main_screen()
            else:
                sys.exit(app.exec_())
             
    def end_game(self):
        # type () -> None
        """ End the game, prompt the player to share strategies and close application """
        self.DataContainer.save_results()

        if self.turns == 0 :
            grade = 0 
        else :
            grade = self.points/self.turns
        ret = QtWidgets.QMessageBox.information(self, "Game ended", "Good job! Thank you! Your probablity of success has reached: "
                                        + str(format(grade, '.2f')) + ". Do you want to share your strategies?", QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
        if ret == QtWidgets.QMessageBox.No:
            if self.StrategiesInput.toPlainText():
                self.DataContainer.append_strategies(self.current_game_mode,self.doping, 
                                                 self.StrategiesInput.toPlainText(),
                                                 self.turns, self.points)
            answer = QtWidgets.QMessageBox.information(self, "Game ended", "Would you like to return to the main menu?", QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
            if answer == QtWidgets.QMessageBox.Yes:
                self.go_to_main_screen()
            else:
                sys.exit(app.exec_())
        else :
            self.write_strategies(game_finished=True)
        

""" INITIALIZATION """            
class InitDialog(QtWidgets.QDialog, Ui_InitDialog):
    def __init__(self):
        super().__init__()
        QtWidgets.QDialog.__init__(self)        
        Ui_InitDialog.__init__(self)
        self.setupUi(self)
        self.setWindowTitle("Welcome!")
        self.Ok_Button.clicked.connect(self.close)
        self.textBrowser.setOpenExternalLinks(True) # Allow to open external link 
        

""" MAIN CALL """
if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = MainWindow()
    window.show()
    window_init = InitDialog()
    window_init.exec()
    window_init.activateWindow()
    sys.exit(app.exec_())