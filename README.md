# Quantum Mønster

For further development of the game Quantum Mønster.

The data generated from playing the game is saved as a .JSON file. Provided that the same name is provided again, the previous data file will be loaded and new data appended to it.

Guesses and answers are noted by the following numbers in the data file: 

0 - Pi-flux, 1 - Strings, 2 - Sprinkled holes, 3 - T=0.1 J, 4 - T=0.8 J 

Submitted player strategies and insights are saved / appended to a .txt file along with relevant data about the current game play.

# Requirements

The game is tested to work on Python 3.7.2 and uses the following packages:
- matplotlib v. 3.1.1
- numpy v. 1.16.4
- pillow v. 8.1.0
- PyQt5 v. 5.15.2

# Known bugs

**ModuleNotFoundError**: No module named 'numpy.core.multiarray\r'

This error is caused by the differences in line-endings between unix and windows.

It is fixed by overwriting all the *.pkl and *.pickle files with those found in the zipped files with the same name.

Note also that if running from **Spyder**, the game may hang upon quitting. The data is still saved, simply force quit the program and re-run it!
