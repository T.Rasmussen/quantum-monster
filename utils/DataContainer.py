""" 
Date created: 27-2-2021
Author: Tobias Rasmussen, 201608265@post.au.dk
"""
import json
import os.path
import utils.GameConstants as GameConstants
from copy import deepcopy

class DataContainer():
    """ 
    Responsible for saving player results and submitted insights 
    Results from different game modes are saved as a JSON document
    Insights and strategies are saved as a seperate .txt file
    """ 

    def __init__(self):
        # type () -> None
        """ Constructor method """
        self.player_name= "unknown_player" # type : str
        self.player_is_physicist = False # type : bool
        self.player_physicist_level = None # type : str or None
        self.runs = {} # type : dict. Number of games played for each game mode, key GAMEMODE : doping,  value: runs (int)
        self.player_results = {} # type : dict. with keys (GAMEMODE, doping) and values {0 : {"score":score, guesses:[01210], answers:[0121] } , 1: {}, 2:... for each run}
        self.player_strategies = {} # type : dict. keys are (GAMEMODE, doping) and value string
        self.player_drawings = {} # type : dict. Submitted drawings, keys THEORY and values list of drawings 

    def check_for_results(self) -> bool:
        """ Check if there are player sessions saved """
        for file in os.listdir("./results/"):
            if file[-5:] == ".json":
                return True
        return False
    
    def get_old_session_names(self) -> list:
        """ Return the names of players with saved results """
        players = []
        for file in os.listdir("./results/"):
            if file[-5:] == ".json":
                players.append(file[:-13])
        return players

    def load_previous_result(self, name : str) -> None:
        """ Load datafile from previous session """
        filename = "./results/" + name + "_results.json"
        if os.path.exists(filename):
            with open(filename) as f:
                data = json.load(f)
            self.player_name = data["name"]
            self.player_is_physicist = data["physicist"]
            self.player_physicist_level = data["physicist_level"]
            try:
                self.player_results = data["results"]
            except KeyError:
                pass
            try:
                self.player_drawings = data["drawings"]
            except KeyError:
                pass
            try:
                self.runs = data["runs"]
            except KeyError:
                pass

    def append_result(self, game_mode : str, doping : float, guess : int, answer : int, snapshot_size : int) -> None:
        """ 
        Append results to data of current run in the game mode played. 
        If no prior data for this game mode, a new one is made 
        """
        game_mode_is_doping_ind = GameConstants.GAME_MODE_DOPING_INDEPENDENT[game_mode] # type : bool
        # Append results
        if game_mode_is_doping_ind:
            results_dict = self.player_results[game_mode][self.runs[game_mode]]
        else:
            results_dict = self.player_results[game_mode][str(doping)][self.runs[game_mode][str(doping)]]
        results_dict["guesses"].append(guess)
        results_dict["answers"].append(answer)
        results_dict["snapshot_size"] = snapshot_size

    def append_drawing(self, drawing, theory):
        # type (drawing : list, theory : str) -> None
        """ Append the drawing to list of submitted drawings for different theories """
        try:
            self.player_drawings[theory]
        except KeyError:
            self.player_drawings[theory] = {}
        self.player_drawings[theory][len(self.player_drawings[theory])] = drawing

    def save_results(self) -> None:
        # type () -> None
        """ Write all gathered data into one big dictionary and save the file as a JSON file """
        # Collect everything into one large dict
        Results = {
            "name" : self.player_name,
            "physicist" : self.player_is_physicist,
            "physicist_level" : self.player_physicist_level
            }
        if self.player_results:
            Results["results"] = self.player_results
        if self.player_drawings:
            Results["drawings"] = self.player_drawings
        if self.runs:
            Results["runs"] = self.runs
        # Save as json object
        filename = "./results/" + self.player_name + "_results.json"
        with open(filename, 'w', encoding='utf-8') as f:
            json.dump(Results, f, ensure_ascii=False, indent=4)
        
    def append_strategies(self, game_mode, doping, strategies, turns, points):
        # type (game_mode : str, doping : float, strategies : str, turns : int, points : int) -> None
        """ Append submitted strategies to player strategies document and save it """
        filename = "./results/" + self.player_name + "_strategies.txt"
        game_mode_is_doping_ind = GameConstants.GAME_MODE_DOPING_INDEPENDENT[game_mode] # type : bool

        with open(filename, mode="a") as f:
            f.write("Game mode: " + game_mode + "\n")
            f.write("Doping level: " + str(doping) + "\n")
            if game_mode_is_doping_ind:
                try:
                    f.write("run number: " + str(self.runs[game_mode]) + "\n")
                except KeyError:
                    f.write("run number: 0 \n")
            else:
                try:
                    f.write("run number: " + str(self.runs[game_mode][doping]) + "\n")
                except KeyError:
                    f.write("run number: 0 \n")
            f.write("turns: " + str(turns) + "\n")
            f.write("points: " + str(points) + "\n")    
            f.write("input:\n")
            f.write(strategies)
            f.write("\n\n")
        
    def count_up_run(self, game_mode : str, doping : float) -> None:
        """ 
        Add one to the current number of runs for the given game mode if it has been played 
        If it has not been played, then prepare the entry to save future results
        """
        new_player_results_dict = {"guesses" : [], "answers" : []}

        if GameConstants.GAME_MODE_DOPING_INDEPENDENT[game_mode]:
            if game_mode in self.runs:
                self.runs[game_mode] += 1
            else:
                self.runs[game_mode] = 1
                self.player_results[game_mode] = {}
            self.player_results[game_mode][self.runs[game_mode]] = new_player_results_dict
        else:
            if game_mode in self.runs:
                if str(doping) in self.runs[game_mode]:
                    self.runs[game_mode][str(doping)] += 1
                else:
                    self.runs[game_mode][str(doping)] = 1
                    self.player_results[game_mode][str(doping)] = {}
            else:
                self.runs[game_mode] = {str(doping) : 1}
                self.player_results[game_mode] = {str(doping) : {} }
            self.player_results[game_mode][str(doping)][self.runs[game_mode][str(doping)]] = new_player_results_dict
        
