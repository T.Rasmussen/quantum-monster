""" 
Date created: 19-2-2021
Author: Tobias Rasmussen, 201608265@post.au.dk
"""

""" Contains all relevant constants of the game """
ANSWER_BUTTON_SIZE = (200,70)
ANSWER_BUTTON_STYLE = "font-size: 24px;background: white; color:black" # Answer button in Comparison and Random mode
ALT_MODE_REGULAR_COLORS = ["rgb(253,102,102)", "rgb(15,128,255)", "rgb(254,204,102)"] # Works like [hole, up, down] since idx are [0, 1, -1]
ALT_MODE_DEVIATION_COLORS = ["rgb(253,102,102)", "rgb(102, 204, 255)", "rgb(102, 153, 51)"] # [hole, same, different]
ALT_MODE_DRAWING_DIMENSIONS = 6
BUTTON_STYLE_TOGGLED = "font-size: 24px;background: #A0A0A0; color:black"
BUTTON_STYLE_NOT_TOGGLED = "font-size: 24px;background: #FFFFFF; color:black" 
CHANGE_RES_BUTTON_NOT_TOGGLED = "font-size: 20px; background: #FFFFFF; color:black"
CHANGE_RES_BUTTON_TOGGLED = "font-size: 20px; background: #C0C0C0; color:black"
COMPARISON_BUTTON_SIZE = (220,72)
COMPARISON_BUTTON_STYLE = "font-size: 28px;background: white; color:black"
DEVIATION_COLOR_DIFFERENT = [102, 153, 51] # Dark green
DEVIATION_COLOR_SAME = [102, 204, 255] # Light blue
THEORY_BUTTON_SIZE = (100,60)
THEORY_BUTTON_STYLE = "font-size: 20px;background: white; color:black"
EXAMPLE_MODE_POPUP_MOVE = (170,150)
EXAMPLE_MODE_POPUP_SIZE = (450, 150)
EXAMPLE_MODE_POPUP_STYLE = "font-size: 20px; border:1px solid black; color:black"
DRAW_MODE_BUTTON_SIZE = (125, 50)
DRAW_MODE_BUTTON_STYLE = "font-size: 28px;background: white; color:black"
DRAW_MODE_LABEL_MOVE = (170, 500)
DRAW_MODE_LABEL_SIZE = (450, 60)
DRAW_MODE_LABEL_STYLE = "font-size: 24px; border:1px solid black; background: white; color:black"
GAME_BUTTON_SIZE = (200,20)
GAME_BUTTON_STYLE = "font-size: 18px;background: white; color:black"
GAME_FEEDBACK_SIZE = (200,72)
INPUT_LINE_SIZE = (320,40)
INPUT_LINE_STYLE = "font-size: 18px;background: white; color:black"
MODE_BUTTON_SIZE = (220,80)
MODE_BUTTON_STYLE = "font-size: 24px;background: white; color:black" # Button for choosing game mode on main screen
LEVEL_BUTTON_SIZE = (140,80)
LEVEL_BUTTON_STYLE = "font-size: 24px;background: white; color:black"
SINGLE_IMAGE_RECTANGLE = (100, 15, 600, 400) # For use in QLabel.setGeometry( QtCore.QRect(x) )
LEFT_EXAMPLE_RECTANGLE = (0, 40, 400, 400)
RIGHT_EXAMPLE_RECTANGLE = (400, 40, 400, 400)
LEFT_IMAGE_RECTANGLE = (15, 120, 380, 250)
RIGHT_IMAGE_RECTANGLE = (405, 120, 380, 250)
SNAPSHOT_COLOR_DOWN = [254,204,102] # Yellow ish
SNAPSHOT_COLOR_HOLE = [253,102,102] # Red ish
SNAPSHOT_COLOR_UP = [15,128,255] # Blue ish
SNAPSHOT_COLORS = [SNAPSHOT_COLOR_DOWN, SNAPSHOT_COLOR_HOLE, SNAPSHOT_COLOR_UP]
THEORYCODES = [0, 1, 2, 3, 4] # 0:piflux, 1:string, 2:random/sprinkled holes, 3:T=0.1J, 4:T=0.8J
PIFLUX_STRING = "Pi-Flux"
STRINGS_STRING = "Strings"
SPRINKLED_STRING = "Sprinkled holes"
T01J_STRING = "T = 0.1 J"
T08J_STRING = "T = 0.8 J"
THEORYDICT = {0:PIFLUX_STRING, 1:STRINGS_STRING, 2:SPRINKLED_STRING, 3:T01J_STRING, 4:T08J_STRING}
GAME_MODE_DRAWING = "drawing"
GAME_MODE_CLASSIC = "classic"
GAME_MODE_CLASSICPLUS = "classicplus"
GAME_MODE_HEISENBERG = "heisenberg"
GAME_MODE_EXAMPLES = "examples"
GAME_MODE_COMPARISON = "comparison"
GAME_MODE_DICT = {  GAME_MODE_CLASSIC:[0, 1], GAME_MODE_CLASSICPLUS:[0,1,2], GAME_MODE_HEISENBERG:[3,4],
                    GAME_MODE_EXAMPLES:THEORYCODES, GAME_MODE_COMPARISON:[0,1], GAME_MODE_DRAWING:THEORYCODES} # Choosable theories of each game mode
GAME_MODE_LIST = [GAME_MODE_DRAWING, GAME_MODE_CLASSIC, GAME_MODE_COMPARISON, GAME_MODE_EXAMPLES, GAME_MODE_HEISENBERG, GAME_MODE_CLASSICPLUS]
DOPING_LEVELS = [4.5, 9.0]
GAME_MODE_DOPING_INDEPENDENT = {GAME_MODE_CLASSIC : False, GAME_MODE_COMPARISON : False, GAME_MODE_DRAWING : False, 
                                GAME_MODE_EXAMPLES : True, GAME_MODE_HEISENBERG : True, GAME_MODE_CLASSICPLUS : True}
LOAD_GAME_BUTTON_SIZE = (200, 75)
LOAD_GAME_BUTTON_STYLE = "font-size: 20px; color:black; background: white;"
LOAD_GAME_BUTTON_STYLE_NOT_ENABLED = "font-size: 20px; color:black; background: grey;"  