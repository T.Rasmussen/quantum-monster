#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 26 15:24:09 2020

@author: diakhere
Author: Tobias Rasmussen, 201608265@post.au.dk
"""

import numpy as np
import matplotlib.pyplot as plt
import random
from PIL import Image
import pickle

import utils.GameConstants as GameConstants

class Generator():
    """ Generator class for generating images """
    def __init__(self):
        self.cropped_size= 8 # type : int. Cropped image dimensions
        self.seen_images = {x:[] for x in GameConstants.THEORYCODES} # type : dict. Images for each theory already seen
        self.strings_distribution = {} # type : dict
        self.snapshot_size = (768, 768) # type : tuple
        self.number_of_example_images = 1 # Images shown in example mode

    def generate_images(self, theory : int, doping : float) -> tuple:
        """ Generate a QPixmap of snapshot with "theory" with doping level "doping" and the deviation from an AFM of this snapshot """
        data = self._get_data(theory, doping) # type : np.array
        RegularImage = self.make_regular_image(data)
        DeviationImage = self.make_deviation_image(data)
        return self.paste_image_on_background(RegularImage), self.paste_image_on_background(DeviationImage)

    def generate_example_image(self, theory : int, doping : float) -> tuple:
        """ Generate a number of images of the theory chosen, placing them in a convinient way """
        WhiteBackground = Image.open("./ui/white_background.png")
        FinalRegImg = WhiteBackground.copy()
        FinalDevImg = WhiteBackground.copy()
        final_image_width = GameConstants.LEFT_EXAMPLE_RECTANGLE[2]
        final_image_height = GameConstants.LEFT_EXAMPLE_RECTANGLE[3]
        FinalRegImg = FinalRegImg.resize(size=(final_image_height, final_image_width), resample=Image.NEAREST)
        FinalDevImg = FinalDevImg.resize(size=(final_image_height, final_image_width), resample=Image.NEAREST)

        if self.number_of_example_images < 5:
            image_spacing = 25 # Spacing between edges and between snapshots, in pixels
            columns = 1 if self.number_of_example_images == 1 else 2
        else:
            image_spacing = 10 
            columns = 4 if self.number_of_example_images == 16 else 5

        snapshot_size = (final_image_width - image_spacing)//columns - image_spacing
        next_snapshot_position = [image_spacing, image_spacing] # x,y of next snapshot paste
        row = 0
        for i in range(self.number_of_example_images):
            # Generate Image
            data = self._get_data(theory, doping) # type : np.array
            RegImg = self.make_regular_image(data)
            DevImg = self.make_deviation_image(data)
            # Resize and paste
            RegImg = RegImg.resize((snapshot_size, snapshot_size), Image.NEAREST)
            DevImg = DevImg.resize((snapshot_size, snapshot_size), Image.NEAREST)
            FinalRegImg.paste(RegImg, tuple(next_snapshot_position))
            FinalDevImg.paste(DevImg, tuple(next_snapshot_position))
            # Update positions
            if (i + 1) % columns == 0: # Next row 
                row += 1
                next_snapshot_position[0] = image_spacing
                next_snapshot_position[1] = image_spacing + (snapshot_size + image_spacing) * row
            else: # Next column
                next_snapshot_position[0] += image_spacing + snapshot_size

        return FinalRegImg, FinalDevImg

    def make_regular_image(self, data : np.array) -> Image.Image: 
        """ Make an image with regular colors"""
        colors = np.zeros((16,16,3), dtype=np.uint8) # matrix of [R, G, B] values
        for x in range(16): 
            for y in range(16):
                n = int(data[x,y]) # N is one of -1, 0, +1
                colors[x,y] = GameConstants.SNAPSHOT_COLORS[n + 1]
        return self.make_image_from_colors(colors)
        
    def make_deviation_image(self, data):
        # type (data : np.array) -> Image
        """ Make a color matrix with deviation colors"""
        colors = np.zeros((16,16,3), dtype=np.uint8) # matrix of [R, G, B] values
        afm_is_type1 = self.check_afm_type(data) # type : bool. True if blue(+1) in main diag, False if yellow(-1)
        for x in range(16): 
            for y in range(16):
                n = int(data[x,y]) # N is one of -1, 0, +1
                if n == 0:
                    colors[x,y] = GameConstants.SNAPSHOT_COLOR_HOLE
                else:
                    afm = (x + y + 1) % 2 if afm_is_type1 else ( (x + y) % 2) - 1 # See check_afm_type
                    if (n == afm or n == afm - 1) and afm_is_type1:
                        colors[x,y] = GameConstants.DEVIATION_COLOR_SAME
                    elif (n == afm or n == afm + 1) and not afm_is_type1: 
                        colors[x,y] = GameConstants.DEVIATION_COLOR_SAME
                    else:
                        colors[x,y] = GameConstants.DEVIATION_COLOR_DIFFERENT
        
        return self.make_image_from_colors(colors)

    def check_afm_type(self, data):
        # type (data : np.array) -> bool
        """ 
        Run through data, checking the number of 'correct' particles for each of the 2 AFMs 
        Return True if most similar AFM is blue (spin +1) in upper right corner and False if yellow (spin -1)
        """
        type_1, type_2 = 0,0 # blue, yellow in upper right corner respectively

        for x in range(16): 
            for y in range(16):
                n = int(data[x,y]) # N is one of -1, 0, +1
                if n != 0:
                    afm_1 = (x + y + 1) % 2 # Reference AFM type 1 at site, 0=up(+1), 1=down(-1)
                    afm_2 = ( (x + y) % 2 ) - 1
                    if n == afm_1 or n == afm_1 - 1:
                        type_1 += 1
                    elif n == afm_2 or n == afm_2 + 1:
                        type_2 += 1

        return type_1 >= type_2 # true if type 1 has more correct fermions than type 2

    def make_image_from_colors(self, colormatrix : np.array) -> Image.Image:
        """ Make an image from a matrix of RGB colors, crops it and resizes it"""
        Img = Image.fromarray(colormatrix) # Create a PIL image
        # Crop image
        size_leftTop = [8 - self.cropped_size // 2] * 2
        size_rightBottom = [8 + self.cropped_size // 2] * 2
        size = tuple(size_leftTop + size_rightBottom)
        Img = Img.crop(size) # im.crop((left, top, right, bottom))
        Img = Img.resize(self.snapshot_size, resample=Image.NEAREST) # Resize image with no blur
        return Img
       
    def paste_image_on_background(self, Img : Image.Image) -> Image.Image:
        """ Pastes argument Img onto a white background and returns it """
        WhiteBackground = Image.open('./ui/white_background.png')
        FinalImg = WhiteBackground.copy()
        FinalImg.paste(Img, (216,16))
        return FinalImg

    def find_string_patterns(self, theory, doping):
        # type (theory : int, doping : float) -> None
        """ 
        Calculate the distribution of string patterns as a function of length of the current snapshot 
        Algorithm is based on [arXiv:1810.03584]
        """
        # Load data
        n = self.seen_images[theory][-1]
        data = self._get_data(theory, doping, n) # type : np.array
        # Remove one spin species (least popular)
        if np.sum(data) < 0:
            dominant_spin = -1
        else:
            dominant_spin = 1
        # Identify correct AFM model and detect deviations from this
        afm_type = self.check_afm_type(data) # True if +1 in diag, False if -1 in diag
        defects = np.zeros_like(data, dtype=int) # 0 at (x,y) means no defect, else 1
        # dom_site marks idx of a site where the dominant spin should be when checking using [x+y]%2
        dom_spin_site = 0 if (dominant_spin == 1 and afm_type) or (dominant_spin == -1 and not afm_type) else 1
        empty_sites = []
        for x in range(16):
            for y in range(16):
                if (x + y) % 2 == dom_spin_site:
                    if data[x,y] != dominant_spin: # mark dominant sites w/o dominant spin species
                        defects[x,y] = 1
                else:
                    if data[x,y] == dominant_spin: # Mark dominant spin species outside dominant sites
                        defects[x,y] = 1
                if data[x,y] == 0:
                    defects[x,y] = 1
                    empty_sites.append((x,y)) # Note this site since strings can only start at holes
        # Calculate strings from defects
        string_lenghts = {}
        while empty_sites:
            # Find all strings in all directions from current number of holes
            biggest_strings_found = {} # hole {(x,y) : (length, direction)} direction means [0, 1], [-1, 0] etc.)
            for site in empty_sites:
                best_string_length = 0
                best_string_dir = [0,0]
                for direction in [[1, 0], [0,1], [-1,0], [0,-1]]: # Right, Up, left, down
                    length = 0
                    next_site = np.array(site)
                    while defects[next_site[0],next_site[1]]:
                        length += 1
                        next_site += direction
                        if next_site[0] < 0 or next_site[0] > 15 or next_site[1] < 0 or next_site[1] > 15:
                            break # Dont search out of bounds

                    if length > best_string_length:
                        best_string_length = length
                        best_string_dir = direction
                if best_string_length > 0:
                    biggest_strings_found[site] = (best_string_length, best_string_dir)
            # Remove biggest string found from defects and count it up
            if len(biggest_strings_found) == 0:
                break
            else:
                longest_string_found = 0
                longest_string_site = 0
                for (k,v) in biggest_strings_found.items():
                    if v[0] > longest_string_found:
                        longest_string_found = v[0]
                        longest_string_site = k
                # Extract info about biggest string        
                direction = biggest_strings_found[longest_string_site][1]
                opposite_direction = list(np.array(direction, dtype=int) * -1)
                other_directions = [[0,1], [0,-1], [1, 0], [-1, 0]]
                other_directions.remove(direction)
                other_directions.remove(opposite_direction)
                # Firstly remove potential site behind starting site (only done once)
                longest_string_site = np.array( longest_string_site, dtype=int )
                site = np.array(longest_string_site, dtype=int)
                site += opposite_direction
                if 0 <= site[0] < 16 and 0 <= site[1] < 16:
                    if defects[site[0], site[1]] != 0:
                        defects[site[0], site[1]] = 0
                        try:
                            empty_sites.remove(tuple(site))
                        except ValueError:
                            pass
                site += direction # Move to starting site

                # remove string from defects
                while defects[site[0], site[1]]:
                    # remove nearby strings in other directions, e.g. left/right if dir=up
                    for other_direction in other_directions:
                        site += other_direction
                        if 0 <= site[0] < 16 and 0 <= site[1] < 16:
                            if defects[site[0], site[1]] != 0:
                                defects[site[0], site[1]] = 0
                                try:
                                    empty_sites.remove(tuple(site))
                                except ValueError:
                                    pass
                        site -= other_direction

                    defects[site[0], site[1]] = 0
                    try: # Try to remove site if it is in list of empty sites
                        empty_sites.remove( tuple( site ) )
                    except ValueError: # Move on if not in list
                        pass
                    site += direction # Step forward and assert this is "legal"
                    if site[0] < 0 or site[0] > 15 or site[1] < 0 or site[1] > 15:
                        break
                
                if longest_string_found in string_lenghts:
                    string_lenghts[longest_string_found] += 1
                else:
                    string_lenghts[longest_string_found] = 1

        self.strings_distribution = string_lenghts

    def make_string_pattern_barchart_Image(self):
        # type () -> None
        """ Make a barchart of the current string distribution and return it as an Image """
        if self.strings_distribution == {}:
            raise ValueError("No string distribution available")
        fig, ax = plt.subplots()

        lengths = list(self.strings_distribution.keys())
        counts = list(self.strings_distribution.values())
        
        ax.bar(lengths,counts)
        ax.set_title("String-pattern length distribution")
        ax.set_xlabel("String-pattern length")
        ax.set_xticks([n for n in range(min(lengths), max(lengths) + 1)])
        ax.set_xticklabels([str(n) for n in range(min(lengths), max(lengths) + 1)])
        ax.set_ylabel("Number of strings")
        filename = "./results/distribution.png"
        fig.savefig(filename)

    def make_string_pattern_barchart_compMode_image(self, left_distr : dict, right_distr : dict):
        """ Generates a single barchart for 2 different distributions used in comparison mode """
        fig, ax = plt.subplots()

        min_length = 10
        max_length = 1
        bar_width = 0.35
        for (distr,label) in [(left_distr, 'Left image'), (right_distr, 'Right image')]:    
            lengths = list(distr.keys())
            counts = list(distr.values())
            min_length = min(min(lengths), min_length)
            max_length = max(max(lengths), max_length)
            ax.bar(lengths if label == 'Left image' else np.array(lengths)+bar_width,counts, bar_width, label=label)

        ax.legend(loc="upper right")
        ax.set_title("String-pattern length distributions")
        ax.set_xlabel("String-pattern length")
        ax.set_xticks([n + bar_width / 2 for n in range(min_length, max_length + 1)])
        ax.set_xticklabels([str(n) for n in range(min_length, max_length + 1)])
        ax.set_ylabel("Number of strings")
        filename = "./results/distribution.png"
        fig.savefig(filename)

    def _get_data(self, theory, doping, n=None):
        # type (theory : int, doping : float, n : int or None) -> np.array
        """ Return filename of theory with doping level """
        if theory == 0: # pi flux
            filepath = "./data/fermisea_d"+str(doping)+"_fullinfo.pickle"
        elif theory == 1: # strings
            filepath = "./data/AS_T0.6_d"+str(doping)+"_nodh_fullinfo.pkl"
        elif theory == 2: #random / sprinkled holes
            filepath = "./data/sprinkling_d9.0_nodh_fullinfo.pkl"
        elif theory == 3: # T=0.1 J
            filepath = "./data/QMC_T0.1.pkl"
        elif theory == 4: # T=0.8 J
            filepath = "./data/QMC_T0.8.pkl"
        else:
            raise IndexError("Could not find data for theory code: "+str(theory))

        file_handle_obj = open(filepath, "rb") # Open file in "read bytes" mode
        depickled_file = pickle.load(file_handle_obj, encoding="bytes") # Depickle
        if n is None: # Generate n if not specified
            n = np.random.randint(0, 2500)
            while n in self.seen_images[theory]:
                n = np.random.randint(0, 2500)
            self.seen_images[theory].append(n)

        if theory == 0:
            data = depickled_file[n]
        else:
            m = random.randint(1,2) # Due to weird data list [[],[data],[data]]
            data = depickled_file[m][n]
        return data
    
    def set_cropping_dimension(self, dimension : int) -> None:
        self.cropped_size = dimension 